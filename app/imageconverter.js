let fs = require('fs');
let thumbnail = require("easyimage").thumbnail;

// function to encode file data to base64 encoded string
module.exports =async function (file) {
    let newName =  process.cwd() +"/passport/"+ Date.now() + '_' + file.name;
    console.log(newName)
    // fs.rename(file.path, newName, async function (error) {
    //     if(error) throw error;
    //     try {
    //         const thumbnailInfo = await thumbnail({
    //             src: newName,
    //             dst: newName,
    //             width: 150,
    //             height: 150,
    //         });
    //
    //         return data(thumbnailInfo.path)
    //
    //     }
    //     catch (e) {
    //         throw e
    //     }
    //
    // });
    return new Promise(async function (fulfill, reject){

        fs.rename(file.path, newName,async function (err) {
            if(err) return reject(err)
            try {
                const thumbnailInfo = await thumbnail({
                    src: newName,
                    dst: newName,
                    width: 150,
                    height: 150,
                });

                fulfill(`data:${file.type};base64,${fs.readFileSync(thumbnailInfo.path, 'base64')}`);
                fs.unlinkSync(newName)
            }
            catch (e) {
                reject(e);
            }

        })

    });

};