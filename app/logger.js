'use strict';

let log4js = require('log4js');

function logger(appenders) {
	let config;
	if (appenders) {
		config = {
			appenders: {...appenders,out:{type:"console"}, file: { type: 'file', filename: 'app.log' } },
			categories : { default: { appenders: ["file","out","alerts"], level: 'info' } }
		};
	} else {
		config = {
			appenders: {out:{type:"console"}, file: { type: 'file', filename: 'app.log' } },
			categories : { default: { appenders: ["file","out"], level: 'info' } }
		};
	}

    log4js.configure(config);
    return {
        getLogger: function(category) {
            return log4js.getLogger(category);
        }
    };
}

module.exports = logger;
